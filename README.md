# physics-toe/top

## About this project

**top** project is the main entry to others projects of theories group. All projects are hosted on [GitLab](https://www.gitlab.com) service.

**GitLab** is a web-based **[git](https://fr.wikipedia.org/wiki/Git)-repository** manager providing wiki, issue-tracking and CI/CD pipeline features, developed by [GitLab Inc](https://en.wikipedia.org/wiki/GitLab). The service targets Software development life cycle but it can used for others usages including the documentation. The **Microsoft** use case of [GitHub](https://github.com) service for documentation is a good successful example of **git** use. [MicrosoftDocs/azure-docs](https://github.com/MicrosoftDocs/azure-docs) is at the [top](https://octoverse.github.com/projects.html) of active projects on **GitHub**.

For more details about using GitLab service see [user documentation](https://docs.gitlab.com/ee/user/index.html). This link is good [introduction](https://docs.gitlab.com/ee/intro).

## Projects

The purpose of the group [**theories**](https://gitlab.com/theories) is to create a set of coherent theories about physics. This group is divided into subgroups all related directly or indirectly with some physics theories. For example we can find a fully **math** theory in subgroup [maths](https://gitlab.com/theories/maths). These theories can be referred in one or more physics theories (see subgroup [physics](https://gitlab.com/theories/physics)).

You can find the complete list of projects and groups [here](<https://gitlab.com/theories>).

If you want to add new project see **How add new project** section in **README.md** file found in the top project of related subgroup. For example [README.md](https://gitlab.com/theories/physics/candidates-top/blob/master/README.md) of the project [candidates-top](https://gitlab.com/theories/physics/candidates-top).

In the most cases, once the project added, the concerned user become the **Maintainer**, see GitLab [permissions](https://docs.gitlab.com/ee/user/permissions.html) for more details.

## Project organization

All files must be included in [**content folder**](content), only some specific files is in the root, like README.md or Licenses.

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) file for more details.

## Licenses

Any project content in this project is under [**Creative Commons LICENSE**](LICENSE.txt) and any project code content in this project is under [**MIT LICENSE**](LICENSE-CODE.txt).