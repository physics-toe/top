# CONTRIBUTING

This contributing guide is used by multiple projects, you can refer it if you want.

## How contributing

There are different ways to contribute to this project. The easy way is to create an issue. To do this, follow these steps :

1. Go to project
1. Go to Issues>List Menu

    ![issues list](content/CONTRIBUTING/issues-list.png)

1. Click on **New issue**

    ![new issue](content/CONTRIBUTING/new-issue.png)

By default, you don't have the permission to add labels to issue, in this case add a text to title containing the desired labels using format **[label1|label2]**. For example **Spelling mistake in section How contributing [language|improvement]**. The maintainer (or anyone with permissions) can modify the title by deleting the extra text and adding desired labels. The issue will be like this

![labeled issue](content/CONTRIBUTING/labeled-issue.png)

If you have enough skills, you can modify current content or add new. If you want your modifications merged with current content you must send a [merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)

![merge request](content/CONTRIBUTING/merge-request.png)

See section [Advanced contributions](#Advanced-contributions) for more details.

## Advanced contributions

Before contributing, you must [fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) the origin project. To keep the forked project synchronized with the origin, create [new branch](https://docs.gitlab.com/ee/gitlab-basics/create-branch.html) from master. Your modification will be pushed in this new branch.

Once your are satisfied with your modifications, synchronize the master branch with the origin project, create another new branch from master which will contains the merge result between the master and your contribution branch. Create new merge request to merge your contribution in the last created branch. Resolve the conflicts if they exists and send a merge request to origin.

Once the merge request is received in origin project merge requests, it will be processed by the maintainer. The first step in merging is to write some quick previews directly using changes log

![merge request review](content/CONTRIBUTING/merge-request-review.png).

If maintainer think that seems *OK*, a temporary branch will be created using merge request id (the merge request will be closed). This new branch will be named **draft**-{id}, where *id* is the request id. For example, this request

![merge request id](content/CONTRIBUTING/merge-request-id.png)

has *1* as id and the temporary branch will be named **draft-1**. An issue with label

![merge label](content/CONTRIBUTING/merge-label.png)

will be created. It will refer to closed merge request and draft branch.
After discussions and probability helped by GitLab [voting](https://docs.gitlab.com/ee/user/award_emojis.html), the maintainer can push the modification to the master branch.

To keep your forked project synchronized with the origin, you can use GitLab [mirroring feature](https://docs.gitlab.com/ee/workflow/repository_mirroring.html).

To edit a file or add new content in your forked project, you can use integrated GitLab editor.
It's possible to use Visual Studio Code editor, see [Contributing using VSCode](content/CONTRIBUTING-VSCode.md) for more details.

## Markdown

Multiple files use a **Markdown** writing style, specially [GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html)(GFM). GFM can be used when writing issues.
For mathematical expressions, GitLab use [KaTeX](https://github.com/KaTeX/KaTeX) engine. KaTeX supports multiple [LaTeX](https://fr.wikipedia.org/wiki/LaTeX) [functions](https://katex.org/docs/supported.html). You can view the rendered result for any KaTeX expressions using this [website](https://katex.org/#demo).